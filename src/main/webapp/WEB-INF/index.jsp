<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Каталог товаров</title>
    <link href="/css/index.css" rel="stylesheet">
</head>
<body>
<header>
    <ul class="menu">
        <li><a href="index">${user.firstName} ${user.lastName}</a></li>
        <li><a id="logout" href="login">Выход</a></li>
        <li class="cartt"><a href="">Корзина</a></li>
        <span class="badge" id="citem">0</span>
        <li class="clickable"><a href="">Поиск</a></li>
    </ul>
</header>
<h1 id="stick_menu">Категория товаров № 1</h1>

<hr>
<div class="flex-container">
    <c:forEach items="${products}" var="product">
            <div class="flex-item" tabindex=0>
                <img src="/resourse/1.png"/>
                <ul>
                    <li class="item_name">${product.getName()}</li>
                    <li class="item_price">${product.getPrice()}</li>
                </ul>
                <a href="#">X</a>
                <button class="btn">В корзину</button>
            </div>
        </c:forEach>
</div>


<h1 id="stick_menu">Категория товаров № 2</h1>
<hr>
<div class="flex-container">
    <c:forEach items="${products}" var="product">
        <div class="flex-item" tabindex=0>
            <img src="/resourse/1.png"/>
            <ul>
                <li class="item_name">${product.getName()}</li>
                <li class="item_price">${product.getPrice()}</li>
            </ul>
            <a href="#">X</a>
            <button class="btn">В корзину</button>
        </div>
    </c:forEach>
</div>

<h1 id="stick_menu">Категория товаров № 3</h1>
<hr>
<div class="flex-container">
    <c:forEach items="${products}" var="product">
        <div class="flex-item" tabindex=0>
            <img src="/resourse/1.png"/>
            <ul>
                <li class="item_name">${product.getName()}</li>
                <li class="item_price">${product.getPrice()}</li>
            </ul>
            <a href="#">X</a>
            <button class="btn">В корзину</button>
        </div>
    </c:forEach>
</div>

<!--Поиск-->
<div class="popup">
    <div class="message">
        <a class="close" href="">×</a>
        <div class="content1">
            <h1>Поиск</h1>
            <hr>
            <button>Найти</button>
            <div class="flex-container clickable">
               <div id="minPrice">
                 	<span class="text-span">Цена: </span><input type="text" style="border:none" id="price" value="">
                 		</div>
                 		<div id="maxPrice">
                 			<span class="text-span">Название: </span><input type="text" style="border:none" id="productName" value="">
                		</div>
               				<input type="submit" id="searchBtn" value="Найти" />
               				<div id="search-results">
               					<ul class="products">

               					</ul>
               				</div>
                </div>

            </div>
        </div>
    </div>
</div>

<!--Корзина-->
<div class="cart">
    <div class="message">
        <a class="close" href="">×</a>
        <div class="content2">
            <h1>Корзина</h1>
            <hr>
            <button id="btn_clear">Оформить покупку</button>
            <div class="cprice">Сумма товаров</div>
            <div class="dprice">Скидка 10%</div>
            <div class="tprice">Итого</div>
        </div>
    </div>
</div>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="/js/index.js"></script>
<script src="/js/cart.js"></script>
<script>
$(function() {
 $('#searchBtn').click(function(ev) {
    ev.preventDefault();
    let parent = $(ev.target).parent();
    let price = parseInt(parent.find('#price').val()) || 0;
    let query = "" || parent.find('#productName').val();
    parent = parent.parent();
    $.get('/products?q='+query+'&price='+price,function(data) {
        data.forEach(function(element) {
        parent.append('<div class="item" tabindex="0" style="width: 100%;"> <img src="'+element.urlImages+'"> <ul> <li class="item_name">'+element.name+'</li> <li class="item_price">'+element.price+'</li> </ul> <button class="btn" style="display: none;">В корзину</button> </div>');
        });
    });
 });
});
</script>
</body>
</html>