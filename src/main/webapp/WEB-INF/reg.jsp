<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Регистрация</title>
    <link href="css/login.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
  <div id="container">
  <h2>Регистрация</h2>
  <hr>
	<form id="form" action="/reg" method="post">
		<label id="error"></label>
		<input class="input" name="login" type="text" placeholder="Логин" path="login"/>
		<br>
		<input class="input" name="password" type="password" placeholder="Пароль" path="password">
		<br>
		<input class="input" name="repeat" type="password" placeholder="Повторите пароль">
		<label id="repeatError"></label>
		<br>
		<input class="input" name="firstname" type="text" placeholder="Имя">
		<br>
		<input class="input" name="lastname" type="text" placeholder="Фамилия">
		<input id="submit" type="submit" value="Вход" >
    </form>
		<br>
		<p><a href="login">Вход</a></p>
    </div>
</body>
</html>