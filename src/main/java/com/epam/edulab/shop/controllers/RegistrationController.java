package com.epam.edulab.shop.controllers;

import com.epam.edulab.shop.domain.User;
import com.epam.edulab.shop.exception.UserAlreadyExistsException;
import com.epam.edulab.shop.manager.UserManager;
import com.epam.edulab.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("/reg")
public class RegistrationController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserManager userManager;

    @GetMapping
    public String registration() {
        return "WEB-INF/reg.jsp";
    }

    @PostMapping(consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String registrationData(@RequestBody MultiValueMap<String, String> form) throws UserAlreadyExistsException {
        User user = new User(form.getFirst("login"), form.getFirst("password"), form.getFirst("firsname"), form.getFirst("lastname"));
        if (!userService.existsUser(user)) {
            userService.addUser(user);
            userManager.setUser(user);
            return "redirect:/login";
        } else {
            throw new UserAlreadyExistsException();
        }
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public String handleUserAlreadyExistsException(Model model) {
        model.addAttribute("errorUserAlreadyExistsException", "Пользователь с данным логином уже существует");
        return "WEB-INF/reg.jsp";
    }
}
