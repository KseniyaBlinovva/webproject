package com.epam.edulab.shop.controllers;

import com.epam.edulab.shop.domain.Product;
import com.epam.edulab.shop.manager.UserManager;
import com.epam.edulab.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class IndexController {
    @Autowired
    ProductService productService;
    @Autowired
    UserManager userManager;

    @GetMapping({"/", "/index"})
    public String index(Model model) {
        List<Product> products = productService.getAllProducts();
        model.addAttribute("user",userManager.getUser());
        model.addAttribute("products", products);
        List<Product> cartList = productService.getCart();
        return "WEB-INF/index.jsp";
    }
}
