package com.epam.edulab.shop.controllers;

import com.epam.edulab.shop.dao.impl.ProductRepositoryImpl;
import com.epam.edulab.shop.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class SearchController {
    @Autowired
    private ProductRepositoryImpl productRepository;
    @GetMapping("/products")
    public List<Product> search(@RequestParam(required = false,name = "q") String query,
                                @RequestParam(required = false) int price) {
        return productRepository.getAll()
                .stream()
                .filter(product -> byPrice(product,price))
                .filter(product -> byName(product,query))
                .collect(Collectors.toList());
    }

    private boolean byName(Product product, String query) {
        if (query == null || query.equals("")) return true;
        return product.getName().equals(query);
    }

    private boolean byPrice(Product product,int price) {
        if (price == 0 ) return true;
        return product.getPrice() == price;
    }
}

