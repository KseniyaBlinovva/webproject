package com.epam.edulab.shop.controllers;

import com.epam.edulab.shop.domain.Product;
import com.epam.edulab.shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/cart/")
public class CartController {

    @Autowired
    ProductService productsService;

    @GetMapping("addProduct/{id}")
    public List<Product> addToCartById(@PathVariable int id, Model model) {
        productsService.addToCart(id);
        return productsService.getCart();
    }

    @GetMapping("deleteProduct/{id}")
    public List<Product> deleteFromCart(@PathVariable int id, Model model) {
        productsService.deleteFromCart(id);
        return productsService.getCart();
    }
}
