package com.epam.edulab.shop.controllers;

import com.epam.edulab.shop.domain.User;
import com.epam.edulab.shop.exception.InvalidPasswordException;
import com.epam.edulab.shop.exception.UserNotFoundException;
import com.epam.edulab.shop.manager.UserManager;
import com.epam.edulab.shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;


@Controller
public class LoginController {
    @Autowired
    UserManager userManager;
    @Autowired
    UserService userService;

    @GetMapping("/login")
    public String showLogin(Model model) {
        model.addAttribute("user", new User());
        return "WEB-INF/login.jsp";
    }

    @PostMapping("/login")
    public String authorize(@Valid User user, BindingResult bindingResult)
            throws UserNotFoundException, InvalidPasswordException {
        if (bindingResult.hasErrors()) {
            return "WEB-INF/login.jsp";
        }
        User foundUser = userService.authenticate(user);
        userManager.setUser(foundUser);
        return "redirect:/";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:/";
    }

    @ExceptionHandler(UserNotFoundException.class)
    public String handleUserNotFoundException(Model model){
        model.addAttribute("error","loginError");
        model.addAttribute("user",new User());
        return "WEB-INF/login.jsp";
    }

    @ExceptionHandler(InvalidPasswordException.class)
    public String handleInvalidPasswordException(Model model){
        model.addAttribute("error", "passwordError");
        model.addAttribute("user", new User());
        return "WEB-INF/login.jsp";
    }
}

