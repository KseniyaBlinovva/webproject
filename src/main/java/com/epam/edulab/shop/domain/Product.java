package com.epam.edulab.shop.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Product {
    public String name;
    public int price;
    public int id;
    public String urlImages;
    public String text;
}
