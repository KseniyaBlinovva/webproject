package com.epam.edulab.shop.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class User {
    public String firstName;
    public String lastName;
    public String login;
    public String password;
    public int id;

    public User(String login) {
        this.login = login;
    }

    public User() {
    }

    public User(String login, String password, String firstName, String lastName) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
