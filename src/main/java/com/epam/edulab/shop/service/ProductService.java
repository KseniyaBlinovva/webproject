package com.epam.edulab.shop.service;

import com.epam.edulab.shop.dao.ProductRepository;
import com.epam.edulab.shop.dao.impl.ProductRepositoryImpl;
import com.epam.edulab.shop.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    ProductRepository productsRepository;

    @Autowired
    public ProductService(ProductRepositoryImpl product) {
        this.productsRepository = product;
    }

    public void addToCart(long id) {
        productsRepository.addToCart(getProductById(id));

    }
    public void deleteFromCart(long id) {
        productsRepository.deleteFromCart(id);
    }

    public List<Product> getCart() {
        return productsRepository.getCart();

    }

    public List<Product> getAllProducts() {
        return productsRepository.getAll();
    }

    public Product getProductById(long id) {
        return productsRepository.getById(id);
    }
    public List<Product> findProductsByNameAndPrice(String nameToFind, double minPrice, double maxPrice){
        List<Product> searchResults = new ArrayList<>();
        for (Product product : getAllProducts()) {
            double price = product.getPrice();
            if (product.getName().toLowerCase().contains(nameToFind.toLowerCase()) && price >= minPrice && price <= maxPrice)
                searchResults.add(product);
        }
        return searchResults;
    }
    public List<Product> findProductsByName(String nameToFind) {
        List<Product> searchResults = new ArrayList<>();
        for (Product product : getAllProducts()) {
            if (product.getName().toLowerCase().contains(nameToFind.toLowerCase()))
                searchResults.add(product);
        }
        return searchResults;
    }
}
