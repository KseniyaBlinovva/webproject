package com.epam.edulab.shop.service;

import com.epam.edulab.shop.dao.UserRepository;
import com.epam.edulab.shop.domain.User;
import com.epam.edulab.shop.exception.InvalidPasswordException;
import com.epam.edulab.shop.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public  UserService(UserRepository userRepository){
        this.userRepository = userRepository;
    }
    public User findUserbyId(int id){
        return userRepository.getById(id);
    }
    public User findUserbyLogin(String login){
        return userRepository.findUserByLogin(login);
    }
    public boolean existsUser(User user){
        return userRepository.exists(user);
    }

    public void addUser(User user){
        userRepository.createOne(user);
    }
    public User authenticate(User user)throws UserNotFoundException, InvalidPasswordException{
        User foundUser = this.findUserbyLogin(user.getLogin());
        if (foundUser == null){
            throw new UserNotFoundException();
        }
        if(!foundUser.getPassword().equals(user.getPassword())){
            throw new InvalidPasswordException();
        }
        return foundUser;
    }

}
