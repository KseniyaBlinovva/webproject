package com.epam.edulab.shop.config;

import com.epam.edulab.shop.interceptor.AuthInterceprot;
import com.epam.edulab.shop.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class WebConfig extends WebMvcConfigurerAdapter{
    @Autowired
    private UserManager manager;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceprot(manager)).addPathPatterns("/**").excludePathPatterns("/login", "/reg");
    }
}
