package com.epam.edulab.shop.dao;

import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CrudRepository<T> {
    void createOne(T obj);
    void deleteOne(T obj);
    T getById(long id);
    void deleteById(long id);
    void updateOne(T obj);
    void updateById(long id);
    List<T> getAll();

}
