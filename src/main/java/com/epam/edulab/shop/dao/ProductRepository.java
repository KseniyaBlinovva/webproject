package com.epam.edulab.shop.dao;

import com.epam.edulab.shop.domain.Product;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product> {

    void addToCart(Product p);

    List<Product> getCart();

    void deleteFromCart(long id);
}


