package com.epam.edulab.shop.dao.impl;

import com.epam.edulab.shop.container.UserContainer;
import com.epam.edulab.shop.dao.UserRepository;
import com.epam.edulab.shop.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    UserContainer userContainer;

    @Override
    public void createOne(User obj) {
        userContainer.add(obj);

    }

    @Override
    public void deleteOne(User obj) {

    }

    @Override
    public User getById(long id) {
        return userContainer.getUserList().stream().filter(user -> user.getId() == id).findFirst().get();
    }

    @Override
    public void deleteById(long id) {
    }

    @Override
    public void updateOne(User obj) {
    }

    @Override
    public void updateById(long id) {
    }

    @Override
    public List<User> getAll() {
        return userContainer.getUserList();
    }

    @Override
    public User findUserByLogin(String login) {
        return userContainer.getUserList().stream().filter(user -> user.getLogin().equals(login)).findFirst().orElse(null);
    }

    @Override
    public boolean exists(User user) {
        return false;
    }

    @Override
    public void add(User user) {
        userContainer.add(user);
    }
}

