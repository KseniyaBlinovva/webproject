package com.epam.edulab.shop.dao;

import com.epam.edulab.shop.domain.User;

public interface UserRepository extends CrudRepository<User> {

    User findUserByLogin(String login);
    boolean exists(User user);
    void add(User user);

}
