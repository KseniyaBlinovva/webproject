package com.epam.edulab.shop.dao.impl;

import com.epam.edulab.shop.container.ProductContainer;
import com.epam.edulab.shop.container.UserContainer;
import com.epam.edulab.shop.dao.ProductRepository;
import com.epam.edulab.shop.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class ProductRepositoryImpl implements ProductRepository {
    @Autowired
    private ProductContainer productContainer;

    @Override
    public void createOne(Product obj) {
        productContainer.addToProducts(obj);
    }

    @Override
    public void deleteOne(Product obj) {
        productContainer.deleteFromProducts();

    }

    @Override
    public Product getById(long id) {
        return getAll().stream().filter(product -> product.getId() == id).findFirst().orElse(null);
    }

    @Override
    public void deleteById(long id) {
        productContainer.deleteFromProducts();
    }

    @Override
    public void updateOne(Product obj) {
    }

    @Override
    public void updateById(long id) {
    }

    @Override
    public List<Product> getAll() {
        return productContainer.getProductList();
    }

    @Override
    public void addToCart(Product p) {
        productContainer.addToCart(p);

    }

    @Override
    public List<Product> getCart() {
        return productContainer.getCart();
    }

    @Override
    public void deleteFromCart(long id) {
        List<Product> products = getCart();
        for (Product product : products) {
            if (product.getId() == id) {
                products.remove(product);
                return;
            }
        }
    }
}
