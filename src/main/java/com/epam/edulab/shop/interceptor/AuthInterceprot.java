package com.epam.edulab.shop.interceptor;

import com.epam.edulab.shop.manager.UserManager;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AuthInterceprot extends HandlerInterceptorAdapter {
    private UserManager userManager;

    public AuthInterceprot(UserManager userManager) {
        this.userManager = userManager;
    }

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object Handle) throws Exception{
        if(userManager.getUser() == null){
            response.sendRedirect("/login");
            return false;
        }
        return true;
    }
}
