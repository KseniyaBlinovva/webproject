package com.epam.edulab.shop.container;

import com.epam.edulab.shop.domain.User;
import lombok.Data;
import lombok.experimental.Delegate;
import org.springframework.stereotype.Component;

import java.util.concurrent.CopyOnWriteArrayList;

@Data
@Component
public class UserContainer {
    @Delegate
    private CopyOnWriteArrayList<User> userList;

    public UserContainer() {
        this.userList = new CopyOnWriteArrayList<User>();
        String[] login = {"Ar", "San", "Jh"};
        String[] password = {"qwerty", "asd","ponunu"};
        String[] firstName = {"Aria", "Sansa", "Jhon"};
        String[] lastName = {"Stark","Targarien","Snow"};
        for (int i = 0; i < 3; i++) {
            userList.add(new User(login[i],password[i], firstName[i],lastName[i]));
        }
    }

    public CopyOnWriteArrayList<User> getUserList() {
        return userList;
    }

    public void setUserList(CopyOnWriteArrayList<User> userList) {
        this.userList = userList;
    }
}
