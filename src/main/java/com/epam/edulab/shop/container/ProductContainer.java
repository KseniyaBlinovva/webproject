package com.epam.edulab.shop.container;

import com.epam.edulab.shop.domain.Product;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
public class ProductContainer {
    Random random = new Random();
    private List<Product> productList;
    private List<Product> cartList;

    public ProductContainer(){
        String urlImage = "/resourse/1.png";
        String text = "Котик";
        this.productList = new CopyOnWriteArrayList<Product>();
        for(int i =1; i < 11; i++){
            productList.add(new Product("Товар № " + i , random.nextInt(20000),i, urlImage, text));
        }

    }
    public void addToCart(Product product) {
        this.cartList.add(product);
    }

    public void deleteFromProducts() {}

    public void addToProducts(Product product) {
        this.productList.add(product);
    }

    public void setCart(List<Product> cart) {
        this.cartList = cart;
    }

    public List<Product> getCart() {
        return cartList;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

}
